﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;

namespace DeclaracionPatrimonial.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/datosdeclarante")]
    public class DeclaracionController : ApiController
    {
        [HttpGet, Route("general/{id}")]
        public IHttpActionResult declarantedata(int id)
        {
           // var query = new StringBuilder();
            //parameters

            
            

             //string squery = "SELECT documento FROM declaracion_temporal_prueba WHERE id = 1 ";

            //query.Append("SELECT documento FROM declaracion_temporal_prueba WHERE id = @idAct ");
            //var squery = query.ToString();

            // Specify connection options and open an connection
            var strConn = System.Configuration.ConfigurationManager.ConnectionStrings["BintechStr"].ConnectionString;
            NpgsqlConnection conn = new NpgsqlConnection(strConn);
            conn.Open();

            // Define a query
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT documento FROM declaracion_temporal_prueba WHERE id = 1", conn);
            NpgsqlParameter idAct = new NpgsqlParameter("@idAct", NpgsqlDbType.Integer);
            idAct.Value = id;
            cmd.Parameters.Add(idAct);
            cmd.Prepare();
            // Execute a query
            NpgsqlDataReader dr = cmd.ExecuteReader();
            string json = JsonConvert.SerializeObject(dr);

            // Close connection
            conn.Close();

            return Ok(json);
        }

    }
    //api/datosDeclarante/general




    
    
}
